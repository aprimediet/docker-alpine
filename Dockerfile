ARG ALPINE_VERSION=3.20

FROM alpine:${ALPINE_VERSION}
LABEL maintainer="<Aditya Prima> aprimediet@gmail.com"

ARG ALPINE_VERSION=3.20
ARG TZ=Asia/Jakarta

# Set User as root
USER root

# INSTALL BASE DEPENDENCIES
RUN --mount=type=cache,target=/var/cache/apk \
    apk add --update \
    libcap tzdata curl

# SET LOCAL TIMEZONE
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# CLEAN APK CACHES
RUN rm -vrf /var/cache/apk/*